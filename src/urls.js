
const URL_REGISTRO = 'https://estadodecuentaenlinea.stargroup.com.mx:5000/SoftvWCFService.svc'
const URL_REPORTES = 'https://realizarpago.stargroup.com.mx:5050'
const URL_SERVICIO = 'https://realizarpago.stargroup.com.mx:5050/SoftvWCFService.svc'
module.exports = {
  URL_SERVICIO,
  URL_REGISTRO,
  URL_REPORTES
}
