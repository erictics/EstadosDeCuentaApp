import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/login'
import recover from '@/components/recover'
import userHome from '@/components/userHome'
import signIn from '@/components/signIn'
import changeEmail from '@/components/changeEmail'
import ayuda from '@/components/ayuda'
import ayudaRegistro from '@/components/ayudaRegistro'
import recuperar from '@/components/recuperar'
import registro from '@/components/registro'
import recuperartoolbox from '@/components/recuperartoolbox'
import registrotoolbox from '@/components/registrotoolbox'
import ayudatoolbox from '@/components/ayudatoolbox'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: login
    },
    {
      path: '/recover',
      name: 'recover',
      component: recover
    },
    {
      path: '/userhome',
      name: 'userHome',
      component: userHome
    },
    {
      path: '/signin',
      name: 'signIn',
      component: signIn
    },
    {
      path: '/changeemail',
      name: 'changeemail',
      component: changeEmail
    },
    {
      path: '/ayuda',
      name: 'ayuda',
      component: ayuda
    },
    {
      path: '/ayudaregistro',
      name: 'ayudaregistro',
      component: ayudaRegistro
    },
    {
      path: '/recuperar',
      name: 'recuperar',
      component: recuperar
    },
    {
      path: '/registro',
      name: 'registro',
      component: registro
    },
    {
      path: '/registrotbx',
      name: 'registrotoolbox',
      component: registrotoolbox
    },
    {
      path: '/recuperartbx',
      name: 'recuperartoolbox',
      component: recuperartoolbox
    },
    {
      path: '/ayudatbx',
      name: 'ayudatoolbox',
      component: ayudatoolbox
    }
  ]
})
